package ru.t1.chernysheva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationLog();

    @NotNull
    String getApplicationName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getGitBranch();

    @NotNull
    String getGitCommitId();

    @NotNull
    String getGitCommitterName();

    @NotNull
    String getGitCommitterEmail();

    @NotNull
    String getGitCommitMessage();

    @NotNull
    String getGitCommitTime();

    @NotNull
    String getDBUser();

    @NotNull
    String getDBPassword();

    @NotNull
    String getDBUrl();

    @NotNull
    String getDBSchema();

    @NotNull
    String getDBCache();

    @NotNull
    String getDBL2Cache();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

    String getServerHost();

    String getServiceHost();

    String getServicePort();

    @NotNull
    String getDBDriver();

    @NotNull String getDBDialect();

    @NotNull String getDBShowSQL();

    @NotNull String getDBHbm2DDL();

    @NotNull
    String getDBCacheRegion();

    @NotNull
    String getDBQueryCache();

    @NotNull
    String getDBMinimalPuts();

    @NotNull
    String getDBCacheRegionPrefix();

    @NotNull
    String getDBCacheProvider();

}
