package ru.t1.chernysheva.tm.api;

import org.jetbrains.annotations.NotNull;

import javax.jms.MessageListener;

public interface IReceiverService {

    void receive(@NotNull final MessageListener listener);

}
