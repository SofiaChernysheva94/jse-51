package ru.t1.chernysheva.tm.enumerated;

public enum DBTables {

    TM_PROJECT,
    TM_TASK,
    TM_USER,
    TM_SESSION

}
