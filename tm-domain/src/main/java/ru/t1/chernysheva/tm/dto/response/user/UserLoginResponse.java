package ru.t1.chernysheva.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.response.AbstractResultResponse;


@NoArgsConstructor
public final class UserLoginResponse extends AbstractResultResponse {

    @Getter
    @Setter
    @Nullable
    private String token;

    public UserLoginResponse(@Nullable final String token) {
        this.token = token;
    }

    public UserLoginResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
