package ru.t1.chernysheva.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.dto.response.AbstractResponse;
import ru.t1.chernysheva.tm.dto.model.UserDTO;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private UserDTO user;

    public AbstractUserResponse(@Nullable final UserDTO user) {
        this.user = user;
    }

}
