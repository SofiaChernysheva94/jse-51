package ru.t1.chernysheva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractModelDTO implements Serializable {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private Date created = new Date();

}
