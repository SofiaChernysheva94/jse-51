package ru.t1.chernysheva.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectClearResponse extends AbstractProjectResponse {
    public ProjectClearResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

}
