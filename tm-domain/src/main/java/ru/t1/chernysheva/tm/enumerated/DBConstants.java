package ru.t1.chernysheva.tm.enumerated;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class DBConstants {

    public static final String TABLE_PROJECT = "PROJECTS";

    public static final String TABLE_TASK = "TASKS";

    public static final String TABLE_USER = "USERS";

    public static final String TABLE_SESSION = "SESSIONS";

}
