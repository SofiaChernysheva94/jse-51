package ru.t1.chernysheva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAdminLogin();

    @NotNull
    String getAdminPassword();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationLog();

    @NotNull
    String getApplicationName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getGitBranch();

    @NotNull
    String getGitCommitId();

    @NotNull
    String getGitCommitterName();

    @NotNull
    String getGitCommitterEmail();

    @NotNull
    String getGitCommitMessage();

    @NotNull
    String getGitCommitTime();

}
